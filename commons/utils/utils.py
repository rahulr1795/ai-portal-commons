import io
import json
import math
import os
import wget
import numpy as np
from PIL import Image
from commons.exceptions import BadRequest, ServerError, MethodNotAllowed, UnsupportedMediaTypeError
from commons.keys import BOTTOM_RIGHT_KEY, TOP_LEFT_KEY, X_KEY, Y_KEY, LABEL
import cv2

class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """

    def default(self, obj):
        if isinstance(obj, (
        np.int_, np.intc, np.intp, np.int8, np.int16, np.int32, np.int64, np.uint8, np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32,
                              np.float64)):
            return float(obj)
        elif isinstance(obj, (np.ndarray,)):  #### This is the fix
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def download(url, directory):
    file_name = wget.download(url=url, out=directory)
    return file_name


def create_folder(path):
    if not os.path.exists(path):
        os.mkdir(path)


def image_to_numpy(input_image):
    image = Image.open(input_image)
    numpy_image = np.asarray(image)
    return numpy_image


def generate_error_message(errors):
    error_messages = []
    print(errors.items())
    for key, value in errors.items():
        error_messages.append(value)
    message = ",".join(error_messages)
    return message


def get_error(status_code, message):
    if status_code == 400:
        return BadRequest(message=message)
    elif status_code == 500:
        return ServerError(message=message)
    elif status_code == 415:
        return UnsupportedMediaTypeError(message=message)
    elif status_code == 405:
        return MethodNotAllowed(message=message)

def draw_bounding_boxes(numpy_image, bounding_boxes=None):
    if bounding_boxes is not None:
        font = cv2.FONT_HERSHEY_TRIPLEX
    for bounding_box in bounding_boxes:
        v1 = bounding_box[TOP_LEFT_KEY][X_KEY], bounding_box[TOP_LEFT_KEY][Y_KEY]
        v2 = bounding_box[BOTTOM_RIGHT_KEY][X_KEY], bounding_box[BOTTOM_RIGHT_KEY][Y_KEY]
        cv2.rectangle(numpy_image, v1, v2, (0, 0, 255), thickness=3)
        cv2.putText(numpy_image, bounding_box[LABEL], v1, font, 0.8, (255, 0, 0))
    return numpy_image


def crop(numpy_image, bounding_box):
    left = bounding_box[TOP_LEFT_KEY][X_KEY]
    top = math.floor(bounding_box[TOP_LEFT_KEY][Y_KEY])
    right = math.floor(bounding_box[BOTTOM_RIGHT_KEY][X_KEY])
    bottom = math.floor(bounding_box[BOTTOM_RIGHT_KEY][Y_KEY])
    cropped_numpy_image = numpy_image[top:bottom, left:right]
    return cropped_numpy_image


def numpy_to_image(numpy_image):
    pil_img = Image.fromarray(numpy_image)
    img_io = io.BytesIO()
    pil_img.save(img_io, 'JPEG', quality=70)
    img_io.seek(0)
    return img_io
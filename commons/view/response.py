class Response:
    def __init__(self, message, status_code, data):
        self.message = message
        self.status_code = status_code
        self.data = data

    def to_dict(self):
        response = dict(
            status=dict(
                code=self.status_code,
                message=self.message
            ),
        )

        if self.data is not None:
            response['data'] = self.data
        return response

    def make_response(self):
        return self.to_dict(), self.status_code


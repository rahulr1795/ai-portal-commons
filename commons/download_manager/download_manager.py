import wget


class DownloadManager:

    @staticmethod
    def download_zip(url, directory):
        filename = DownloadManager.download(url=url, directory=directory)
        DownloadManager.extract_zip(filename=filename)
        pass

    @staticmethod
    def extract_zip(filename):
        pass

    @staticmethod
    def download(url, directory):
        file_name = wget.download(url=url, out=directory)
        return file_name


from abc import ABCMeta, abstractmethod


class BaseClassifier(metaclass=ABCMeta):

    @abstractmethod
    def load_model(self):
        pass

    @abstractmethod
    def run(self, input_data):
        pass

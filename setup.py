from setuptools import setup

setup(
   name='commons',
   version='1.0',
   description='A useful module',
   author='Rahul R',
   author_email='rahul.r@attinadsoftware.com',
   packages=['commons', 'commons.utils', 'commons.base_classifier', 'commons.decorators', 'commons.keys', 'commons.view', 'commons.exceptions', 'commons.constants'],
   install_requires=['opencv-python', 'numpy', 'pillow', 'wget', 'flask-restplus', 'flask-api']
)
